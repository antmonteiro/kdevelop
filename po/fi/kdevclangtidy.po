# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdev-clang-tidy package.
# Tommi Nieminen <translator@legisign.org>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: kdev-clang-tidy\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2018-09-27 15:55+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: analyzer.cpp:29
#, kde-format
msgid "Clang-Tidy"
msgstr "Clang-Tidy"

#: config/checklistfilterproxysearchline.cpp:23
#, fuzzy, kde-format
#| msgid "Search"
msgctxt "@info:placeholder"
msgid "Search..."
msgstr "Etsi"

#: config/checklistmodel.cpp:65
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@item"
msgid "All checks"
msgstr "Kaikki tarkistukset"

#: config/checksetmanagewidget.cpp:89
#, kde-format
msgctxt "@title:window"
msgid "Enter Name of New Check Set"
msgstr ""

#: config/checksetmanagewidget.cpp:95
#, kde-format
msgctxt "@label:textbox"
msgid "Name:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/checksetmanagewidget.ui:31
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@label:listbox"
msgid "Check set:"
msgstr "Tarkistukset"

#. i18n: ectx: property (toolTip), widget (QPushButton, addCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:48
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@info:tooltip"
msgid "Add check set"
msgstr "Kaikki tarkistukset"

#. i18n: ectx: property (toolTip), widget (QPushButton, cloneCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:58
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@info:tooltip"
msgid "Clone check set"
msgstr "Tarkistukset"

#. i18n: ectx: property (toolTip), widget (QPushButton, removeCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:68
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@info:tooltip"
msgid "Remove check set"
msgstr "Kaikki tarkistukset"

#. i18n: ectx: property (toolTip), widget (QPushButton, setAsDefaultCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:78
#, kde-format
msgctxt "@info:tooltip"
msgid "Set as default"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QPushButton, editCheckSetSelectionNameButton)
#: config/checksetmanagewidget.ui:88
#, kde-format
msgctxt "@info:tooltip"
msgid "Edit name of check set"
msgstr ""

#: config/checksetselectioncombobox.cpp:26
#, kde-format
msgctxt "@item:inlistbox"
msgid "Custom"
msgstr ""

#: config/checksetselectioncombobox.cpp:30
#, kde-format
msgctxt "@item:inlistbox"
msgid "Use default (currently: %1)"
msgstr ""

#: config/checksetselectionlistmodel.cpp:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "%1 (default selection)"
msgstr ""

#: config/clangtidypreferences.cpp:62 config/clangtidyprojectconfigpage.cpp:55
#, fuzzy, kde-format
#| msgid "Clang-Tidy"
msgctxt "@title:tab"
msgid "Clang-Tidy"
msgstr "Clang-Tidy"

#: config/clangtidypreferences.cpp:67
#, fuzzy, kde-format
#| msgid "Configure Clang-Tidy Settings"
msgctxt "@title:tab"
msgid "Configure Clang-Tidy Settings"
msgstr "Clang-Tidyn asetukset"

#. i18n: ectx: property (title), widget (QGroupBox, pathsGroupBox)
#: config/clangtidypreferences.ui:29
#, fuzzy, kde-format
#| msgid "Paths"
msgctxt "@title:group"
msgid "Paths"
msgstr "Sijainnit"

#. i18n: ectx: property (text), widget (QLabel, clangtidyLabel)
#: config/clangtidypreferences.ui:37
#, fuzzy, kde-format
#| msgid "Clang-&tidy executable:"
msgctxt "@label:chooser"
msgid "Clang-&Tidy executable:"
msgstr "&Clang-tidy-ohjelmatiedosto:"

#. i18n: ectx: property (toolTip), widget (KUrlRequester, kcfg_clangtidyPath)
#: config/clangtidypreferences.ui:54
#, fuzzy, kde-format
#| msgid "The full path to the clang-tidy executable"
msgctxt "@info:tooltip"
msgid "The full path to the Clang-Tidy executable"
msgstr "Clang-tidy-ohjelmatiedoston koko polku:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_parallelJobsEnabled)
#: config/clangtidypreferences.ui:72
#, fuzzy, kde-format
#| msgid "Run analysis jobs in parallel"
msgctxt "@option:check"
msgid "Run analysis jobs in parallel"
msgstr "Suorita analyysityöt rinnakkain"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_parallelJobsAutoCount)
#: config/clangtidypreferences.ui:81
#, fuzzy, kde-format
#| msgid "Use all CPU cores"
msgctxt "@option:check"
msgid "Use all CPU cores"
msgstr "Käytä kaikkia keskusyksikköjä"

#. i18n: ectx: property (text), widget (QLabel, parallelJobsFixedCountLabel)
#: config/clangtidypreferences.ui:101
#, fuzzy, kde-format
#| msgid "Maximum number of threads:"
msgctxt "@label:spinbox"
msgid "Maximum number of threads:"
msgstr "Säikeiden enimmäismäärä:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_useConfigFile)
#: config/clangtidyprojectconfigpage.ui:29
#, fuzzy, kde-format
#| msgid "&Use .clang-tidy file(s)"
msgctxt "@option:check"
msgid "&Use .clang-tidy file(s)"
msgstr "Käytä .&clang-tidy-tiedostoja"

#. i18n: ectx: attribute (title), widget (QWidget, checksTab)
#: config/clangtidyprojectconfigpage.ui:43
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@title:tab"
msgid "Checks"
msgstr "Tarkistukset"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: config/clangtidyprojectconfigpage.ui:73
#, fuzzy, kde-format
#| msgid "Includes"
msgctxt "@title:tab"
msgid "Includes"
msgstr "Includes"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: config/clangtidyprojectconfigpage.ui:79
#, fuzzy, kde-format
#| msgid "&Check system headers:"
msgctxt "@option:check"
msgid "&Check system headers:"
msgstr "Tarkista &järjestelmäotsakkeet:"

#. i18n: ectx: property (text), widget (QLabel, headerFilterLabel)
#: config/clangtidyprojectconfigpage.ui:92
#, fuzzy, kde-format
#| msgid "Header fi&lter:"
msgctxt "@label:textbox"
msgid "Header fi&lter:"
msgstr "&Otsakesuodatin:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, kcfg_headerFilter)
#: config/clangtidyprojectconfigpage.ui:105
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Regular expression matching the names of the headers to output diagnostics "
"from. Diagnostics from the main file of each translation unit are always "
"displayed. Can be used together with -line-filter."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: config/clangtidyprojectconfigpage.ui:119
#, fuzzy, kde-format
#| msgid "Extra Parameters"
msgctxt "@title:tab"
msgid "Extra Parameters"
msgstr "Lisäparametrit"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/clangtidyprojectconfigpage.ui:125
#, fuzzy, kde-format
#| msgid "E&xtra parameters:"
msgctxt "@label.textbox"
msgid "E&xtra parameters:"
msgstr "L&isäparametrit:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, kcfg_additionalParameters)
#: config/clangtidyprojectconfigpage.ui:135
#, fuzzy, kde-format
#| msgid ""
#| "<html><head/><body><p>Additional command line options to pass to clang-"
#| "tidy.</p></body></html>"
msgctxt "@info:tooltip"
msgid "Additional command line options to pass to Clang-Tidy."
msgstr ""
"<html><head/><body><p>Clang-Tidylle välitettävät lisäkomentoriviparametrit.</"
"p></body></html>"

#: job.cpp:68
#, kde-format
msgid "Clang-Tidy Analysis"
msgstr "Clang-Tidy-analyysi"

#: job.cpp:142
#, kde-format
msgid "Failed to start Clang-Tidy process."
msgstr "Clang-Tidy-prosessin käynnistys epäonnistui."

#: job.cpp:147
#, fuzzy, kde-format
#| msgid "Clang-tidy crashed."
msgid "Clang-Tidy crashed."
msgstr "Clang-Tidy kaatui."

#: job.cpp:151
#, fuzzy, kde-format
#| msgid "Clang-tidy process timed out."
msgid "Clang-Tidy process timed out."
msgstr "Clang-Tidy-prosessin aikakatkaisu."

#: job.cpp:155
#, fuzzy, kde-format
#| msgid "Write to Clang-tidy process failed."
msgid "Write to Clang-Tidy process failed."
msgstr "Kirjoitus Clang-Tidy-prosessiin epäonnistui."

#: job.cpp:159
#, fuzzy, kde-format
#| msgid "Read from Clang-tidy process failed."
msgid "Read from Clang-Tidy process failed."
msgstr "Luku Clang-Tidy-prosessista epäonnistui."

#~ msgid "Clang-tidy Error"
#~ msgstr "Clang-Tidy-virhe"

#~ msgid "Analyze Current File with Clang-Tidy"
#~ msgstr "Analysoi nykyinen tiedosto Clang-Tidyllä"

#~ msgid "Analyze Current Project with Clang-Tidy"
#~ msgstr "Analysoi nykyinen projekti Clang-Tidyllä"

#~ msgid "Error starting clang-tidy"
#~ msgstr "Virhe käynnistettäessä clang-tidyä"

#~ msgid "No suitable active file, unable to deduce project."
#~ msgstr "Sopivaa aktiivista tiedostoa ei ole: projektia ei voi päätellä."

#~ msgid "Active file isn't in a project"
#~ msgstr "Aktiivinen tiedosto ei kuulu projektiin"

#~ msgctxt "@title:window"
#~ msgid "Test"
#~ msgstr "Testaa"

#~ msgid "Analysis started..."
#~ msgstr "Analyysi käynnistynyt…"

#~ msgctxt "@info:tooltip %1 is the path of the file"
#~ msgid "Re-run last Clang-Tidy analysis (%1)"
#~ msgstr "Suorita viimeisin Clang-Tidy-analyysi (%1) uudelleen"

#~ msgctxt "@info:tooltip"
#~ msgid "Re-run last Clang-Tidy analysis"
#~ msgstr "Suorita viimeisin Clang-Tidy-analyysi uudelleen"

#~ msgid "Analysis completed, no problems detected."
#~ msgstr "Analyysi valmis: ongelmia ei havaittu."

#~ msgid "Compilation database file not found: '%1'"
#~ msgstr "Käännöstietokantatiedostoa ei löytynyt: ”%1”"

#~ msgid "Could not open compilation database file for reading: '%1'"
#~ msgstr "Käännöstietokantatiedostoa ei voitu avata luettavaksi: ”%1”"

#~ msgid "JSON error during parsing compilation database file '%1': %2"
#~ msgstr "JSON-virhe jäsennettäessä käännöstietokantatiedostoa ”%1”: %2"

#~ msgid ""
#~ "JSON error during parsing compilation database file '%1': document is not "
#~ "an array."
#~ msgstr ""
#~ "JSON-virhe jäsennettäessä käännöstietokantatiedostoa ”%1”: tiedosto ei "
#~ "ole taulukko."
